package truss

import (
	"net/http"
)

// HTTPError returns a type satisfying the StatusCoder interface in package
// github.com/go-kit/kit/transport/http.
type HTTPError struct {
	error
	statusCode int
	headers    http.Header
}

func NewHTTPError(err error, code int) HTTPError {
	return NewHTTPErrorHeader(err, code, nil)
}
func NewHTTPErrorHeader(err error, code int, headers http.Header) HTTPError {
	return HTTPError{
		error:      err,
		statusCode: code,
		headers:    headers,
	}
}

func (e HTTPError) StatusCode() int {
	return e.statusCode
}

func (e HTTPError) Headers() http.Header {
	return e.headers
}
