// package truss provides helpers for setting up a truss service.
package truss

import (
	"gitlab.com/tight5/kit/logger"
	"gitlab.com/tight5/kit/metrics"
	"gitlab.com/tight5/kit/reqctx"
)

// Init instruments a service by:
// 1. Starting the statsd client.
// 2. Initing the logger with the service name.
// 3. Setting the request package's SvcKey.
func Init(env, service string) {
	metrics.StartDogstatsd(env, service)
	logger.AddDefaultKeyvals("service", service)
	reqctx.Init(service)
}
