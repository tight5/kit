// Package db provides a thin wrapper to the database/sql package with latency metrics
package db

import (
	"database/sql"
	"strconv"
	"strings"
	"time"

	"gitlab.com/tight5/kit/metrics"

	"context"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/pkg/errors"
)

type Config struct {
	Driver          string        `json:"driver"`
	Host            string        `json:"host"`
	Port            int           `json:"port"`
	User            string        `json:"user"`
	Pass            string        `json:"pass"`
	DBName          string        `json:"db-name"`
	MaxIdleConns    int           `json:"max-idle-conns"`
	MaxOpenConns    int           `json:"max-open-conns"`
	ConnMaxLifetime time.Duration `json:"conn-max-lifetime"`

	SSLMode string `json:"sslmode"`
}

func (c Config) ConnString() (string, error) {
	var conn string
	switch c.Driver {
	case "mysql":
		conn = MySQLURLer(c)
	case "postgres":
		conn = PostgresURLer(c)
	default:
		return "", errors.Errorf("driver %s unsupported", c.Driver)
	}

	if conn == "" {
		return "", errors.New("empty connection string")
	}

	return conn, nil
}

type URLerFunc func(c Config) string

func MySQLURLer(c Config) string {
	return c.User + ":" + c.Pass + "@tcp(" + c.Host + ":" + strconv.Itoa(c.Port) + ")/" + c.DBName
}

func PostgresURLer(c Config) string {
	opts := []string{
		"user=" + c.User,
		"password=" + c.Pass,
		"host=" + c.Host,
		"port=" + strconv.Itoa(c.Port),
		"dbname=" + c.DBName,
		"sslmode=" + c.SSLMode,
	}
	return strings.Join(opts, " ")
}

type DB struct {
	Config Config
	db     *sql.DB
}

func New(c Config) (*DB, error) {
	conn, err := c.ConnString()
	if err != nil {
		return nil, err
	}
	db, err := sql.Open(c.Driver, conn)
	if err != nil {
		return nil, err
	}

	// If MaxOpenConns is greater than 0 but less than the new MaxIdleConns
	// then the new MaxIdleConns will be reduced to match the MaxOpenConns limit
	//
	// If n <= 0, no idle connections are retained.
	db.SetMaxIdleConns(c.MaxIdleConns)

	// If MaxIdleConns is greater than 0 and the new MaxOpenConns is less than
	// MaxIdleConns, then MaxIdleConns will be reduced to match the new
	// MaxOpenConns limit
	//
	// If n <= 0, then there is no limit on the number of open connections.
	// The default is 0 (unlimited).
	db.SetMaxOpenConns(c.MaxOpenConns)

	return &DB{
		db:     db,
		Config: c,
	}, nil
}

// NewMockDB - creates a DB with mocked configs and db, using sqlmock
// to be used for unittests
func NewMockDB() (*DB, sqlmock.Sqlmock, error) {
	dbmocked, mock, err := sqlmock.New()
	return &DB{
		db: dbmocked,
		Config: Config{
			DBName: "mock",
		},
	}, mock, err
}

func (db *DB) BeginTx(ctx context.Context, opts *sql.TxOptions) (*Tx, error) {
	reportCount(ctx, db.Config.DBName, "begin")

	tx, err := db.db.BeginTx(ctx, opts)
	if err != nil {
		return nil, err
	}
	return &Tx{
		driver: db.Config.Driver,
		tx:     tx,
		dbName: db.Config.DBName,
	}, nil
}
func (db *DB) Prepare(ctx context.Context, query string) (*Stmt, error) {
	reportCount(ctx, db.Config.DBName, "prepare", "tx", "false")
	stmt, err := db.db.PrepareContext(ctx, query)
	if err != nil {
		return nil, err
	}
	return &Stmt{
		driver: db.Config.Driver,
		stmt:   stmt,
		dbName: db.Config.DBName,
		query:  query,
	}, nil

}
func (db *DB) Ping(ctx context.Context) error {
	return db.db.PingContext(ctx)
}
func (db *DB) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	defer db.reportLatency(ctx, "exec", query)(time.Now())
	return db.db.ExecContext(ctx, query, args...)
}
func (db *DB) Query(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	defer db.reportLatency(ctx, "query", query)(time.Now())
	return db.db.QueryContext(ctx, query, args...)
}
func (db *DB) QueryRow(ctx context.Context, query string, args ...interface{}) *sql.Row {
	defer db.reportLatency(ctx, "query_row", query)(time.Now())
	return db.db.QueryRowContext(ctx, query, args...)
}
func (db *DB) WithTx(ctx context.Context, txFunc func(tx *Tx) error) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	err = txFunc(tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}
func (db *DB) Close() {
	db.db.Close()
}

type Tx struct {
	driver string
	dbName string
	tx     *sql.Tx
}

func (tx *Tx) Commit() error {
	return tx.tx.Commit()
}
func (tx *Tx) Rollback() error {
	return tx.tx.Rollback()
}
func (tx *Tx) Prepare(ctx context.Context, query string) (*Stmt, error) {
	reportCount(ctx, tx.dbName, "prepare", "tx", "true")
	stmt, err := tx.tx.PrepareContext(ctx, query)
	if err != nil {
		return nil, err
	}
	return &Stmt{
		driver:          tx.driver,
		stmt:            stmt,
		dbName:          tx.dbName,
		query:           query,
		fromTransaction: true,
	}, nil
}
func (tx *Tx) Exec(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	defer tx.reportLatency(ctx, "exec", query)(time.Now())
	return tx.tx.ExecContext(ctx, query, args...)
}
func (tx *Tx) Query(ctx context.Context, query string, args ...interface{}) (*sql.Rows, error) {
	defer tx.reportLatency(ctx, "query", query)(time.Now())
	return tx.tx.QueryContext(ctx, query, args...)
}
func (tx *Tx) QueryRow(ctx context.Context, query string, args ...interface{}) *sql.Row {
	defer tx.reportLatency(ctx, "query_row", query)(time.Now())
	return tx.tx.QueryRowContext(ctx, query, args...)
}

type Stmt struct {
	driver          string
	dbName          string
	query           string
	stmt            *sql.Stmt
	fromTransaction bool
}

func (stmt *Stmt) Close() error {
	return stmt.stmt.Close()
}
func (stmt *Stmt) Exec(ctx context.Context, args ...interface{}) (sql.Result, error) {
	defer stmt.reportLatency(ctx, "exec", stmt.query)(time.Now())
	return stmt.stmt.ExecContext(ctx, args...)
}
func (stmt *Stmt) Query(ctx context.Context, args ...interface{}) (*sql.Rows, error) {
	defer stmt.reportLatency(ctx, "query", stmt.query)(time.Now())
	return stmt.stmt.QueryContext(ctx, args...)
}
func (stmt *Stmt) QueryRow(ctx context.Context, args ...interface{}) *sql.Row {
	defer stmt.reportLatency(ctx, "query_row", stmt.query)(time.Now())
	return stmt.stmt.QueryRowContext(ctx, args...)
}

func (db DB) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, db.Config.Driver+".latency",
		"db_name", db.Config.DBName,
		"method", method,
		"command", queryToCommand(query),
		"tx", "false",
		"stmt", "false",
	)
}

func (tx Tx) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, tx.driver+".latency",
		"db_name", tx.dbName,
		"method", method,
		"command", queryToCommand(query),
		"tx", "true",
		"stmt", "false",
	)
}

func (stmt Stmt) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, stmt.driver+".latency",
		"db_name", stmt.dbName,
		"method", method,
		"command", queryToCommand(query),
		"stmt", "true",
		"tx", strconv.FormatBool(stmt.fromTransaction),
	)
}

func queryToCommand(query string) string {
	command := strings.ToLower(strings.Fields(query)[0])
	switch command {
	case "select", "insert", "update", "delete", "create":
		break
	default:
		command = "bad"
	}
	return command
}

func reportCount(ctx context.Context, dbName, method string, tags ...string) {
	metrics.CounterWithContext(ctx, dbName+".count").With("method", method).With(tags...).Add(1)
}
