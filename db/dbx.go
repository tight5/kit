package db

import (
	"database/sql"
	"strconv"
	"time"

	"gitlab.com/tight5/kit/metrics"

	"context"
	"github.com/DATA-DOG/go-sqlmock"
	"github.com/jmoiron/sqlx"
)

// DBx sqlx DB
type DBx struct {
	Config Config
	db     *sqlx.DB
}

// NewWithSQLX created new DBx, using sqlx
func NewWithSQLX(c Config) (*DBx, error) {
	conn, err := c.ConnString()
	if err != nil {
		return nil, err
	}
	db, err := sqlx.Connect(c.Driver, conn)
	if err != nil {
		return nil, err
	}

	// If MaxOpenConns is greater than 0 but less than the new MaxIdleConns
	// then the new MaxIdleConns will be reduced to match the MaxOpenConns limit
	//
	// If n <= 0, no idle connections are retained.
	db.SetMaxIdleConns(c.MaxIdleConns)

	// If MaxIdleConns is greater than 0 and the new MaxOpenConns is less than
	// MaxIdleConns, then MaxIdleConns will be reduced to match the new
	// MaxOpenConns limit
	//
	// If n <= 0, then there is no limit on the number of open connections.
	// The default is 0 (unlimited).
	db.SetMaxOpenConns(c.MaxOpenConns)

	// SetConnMaxLifetime sets the maximum amount of time a connection may be reused.
	//
	// Expired connections may be closed lazily before reuse.
	//
	// If d <= 0, connections are reused forever.
	if c.ConnMaxLifetime > 0 {
		db.SetConnMaxLifetime(c.ConnMaxLifetime)
	}

	ctx, cnl := context.WithTimeout(context.Background(), time.Second*5)
	defer cnl()
	return &DBx{
		db:     db,
		Config: c,
	}, db.PingContext(ctx)
}

// NewMockSqlXDB - creates a DB with mocked configs and db, using sqlmock - to be used for unittests
func NewMockSqlXDB() (*DBx, sqlmock.Sqlmock, error) {
	dbmocked, mock, err := sqlmock.New()
	return &DBx{
		db: sqlx.NewDb(dbmocked, "mockx"),
		Config: Config{
			DBName: "mockx",
		},
	}, mock, err
}

func (db *DBx) Ping(ctx context.Context) error {
	return db.db.PingContext(ctx)
}

func (db *DBx) BeginTx(ctx context.Context, opts *sql.TxOptions) (*TXx, error) {
	reportCount(ctx, db.Config.DBName, "beginx")

	tx, err := db.db.BeginTxx(ctx, opts)
	if err != nil {
		return nil, err
	}
	return &TXx{
		driver: db.Config.Driver,
		tx:     tx,
		dbName: db.Config.DBName,
	}, nil
}
func (db *DBx) Prepare(ctx context.Context, query string) (*Stmtx, error) {
	reportCount(ctx, db.Config.DBName, "preparex", "tx", "false")
	stmt, err := db.db.PreparexContext(ctx, query)
	if err != nil {
		return nil, err
	}
	return &Stmtx{
		driver: db.Config.Driver,
		stmt:   stmt,
		dbName: db.Config.DBName,
		query:  query,
	}, nil

}
func (db *DBx) NamedExec(ctx context.Context, query string, arg interface{}) (sql.Result, error) {
	defer db.reportLatency(ctx, "namedexec", query)(time.Now())
	return db.db.NamedExecContext(ctx, query, arg)
}
func (db *DBx) NamedQuery(ctx context.Context, query string, arg interface{}) (*sqlx.Rows, error) {
	defer db.reportLatency(ctx, "namedquery", query)(time.Now())
	return db.db.NamedQueryContext(ctx, query, arg)
}
func (db *DBx) QueryRowx(ctx context.Context, query string, args ...interface{}) *sqlx.Row {
	defer db.reportLatency(ctx, "queryrowx", query)(time.Now())
	return db.db.QueryRowxContext(ctx, query, args...)
}
func (db *DBx) Queryx(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error) {
	defer db.reportLatency(ctx, "queryx", query)(time.Now())
	return db.db.QueryxContext(ctx, query, args...)
}
func (db *DBx) Select(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	defer db.reportLatency(ctx, "selectx", query)(time.Now())
	return db.db.SelectContext(ctx, dest, query, args...)
}
func (db *DBx) Get(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	defer db.reportLatency(ctx, "getx", query)(time.Now())
	return db.db.GetContext(ctx, dest, query, args...)
}
func (db *DBx) WithTx(ctx context.Context, txFunc func(tx *TXx) error) error {
	tx, err := db.BeginTx(ctx, nil)
	if err != nil {
		return err
	}
	err = txFunc(tx)
	if err != nil {
		tx.Rollback()
		return err
	}
	return tx.Commit()
}
func (db *DBx) Close() {
	db.db.Close()
}

type TXx struct {
	driver string
	dbName string
	tx     *sqlx.Tx
}

func (tx *TXx) Commit() error {
	return tx.tx.Commit()
}
func (tx *TXx) Rollback() error {
	return tx.tx.Rollback()
}
func (tx *TXx) Prepare(ctx context.Context, query string) (*Stmtx, error) {
	reportCount(ctx, tx.dbName, "preparex", "tx", "true")
	stmt, err := tx.tx.PreparexContext(ctx, query)
	if err != nil {
		return nil, err
	}
	return &Stmtx{
		driver:          tx.driver,
		stmt:            stmt,
		dbName:          tx.dbName,
		query:           query,
		fromTransaction: true,
	}, nil
}
func (tx *TXx) NamedExec(ctx context.Context, query string, arg interface{}) (sql.Result, error) {
	defer tx.reportLatency(ctx, "namedexec", query)(time.Now())
	return tx.tx.NamedExecContext(ctx, query, arg)
}
func (tx *TXx) NamedQuery(ctx context.Context, query string, arg interface{}) (*sqlx.Rows, error) {
	defer tx.reportLatency(ctx, "namedquery", query)(time.Now())
	// @FIXME update this call when it will be fixed in sqlx.tx
	return sqlx.NamedQueryContext(ctx, tx.tx, query, arg)
}
func (tx *TXx) QueryRowx(ctx context.Context, query string, args ...interface{}) *sqlx.Row {
	defer tx.reportLatency(ctx, "queryrowx", query)(time.Now())
	return tx.tx.QueryRowxContext(ctx, query, args...)
}
func (tx *TXx) Queryx(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error) {
	defer tx.reportLatency(ctx, "queryx", query)(time.Now())
	return tx.tx.QueryxContext(ctx, query, args...)
}
func (tx *TXx) Select(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	defer tx.reportLatency(ctx, "selectx", query)(time.Now())
	return tx.tx.SelectContext(ctx, dest, query, args...)
}
func (tx *TXx) Get(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	defer tx.reportLatency(ctx, "getx", query)(time.Now())
	return tx.tx.GetContext(ctx, dest, query, args...)
}

type Stmtx struct {
	driver          string
	dbName          string
	query           string
	stmt            *sqlx.Stmt
	fromTransaction bool
}

func (stmt *Stmtx) Close() error {
	return stmt.stmt.Close()
}

func (stmt *Stmtx) Queryx(ctx context.Context, args ...interface{}) (*sqlx.Rows, error) {
	defer stmt.reportLatency(ctx, "queryx", stmt.query)(time.Now())
	return stmt.stmt.QueryxContext(ctx, args...)
}
func (stmt *Stmtx) QueryRowx(ctx context.Context, args ...interface{}) *sqlx.Row {
	defer stmt.reportLatency(ctx, "queryrowx", stmt.query)(time.Now())
	return stmt.stmt.QueryRowxContext(ctx, args...)
}
func (stmt *Stmtx) Select(ctx context.Context, dest interface{}, args ...interface{}) error {
	defer stmt.reportLatency(ctx, "selectx", stmt.query)(time.Now())
	return stmt.stmt.SelectContext(ctx, dest, args...)
}
func (stmt *Stmtx) Get(ctx context.Context, dest interface{}, args ...interface{}) error {
	defer stmt.reportLatency(ctx, "getx", stmt.query)(time.Now())
	return stmt.stmt.GetContext(ctx, dest, args...)
}

func (db DBx) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, db.Config.Driver+".latency",
		"db_name", db.Config.DBName,
		"method", method,
		"command", queryToCommand(query),
		"tx", "false",
		"stmt", "false",
	)
}

func (tx TXx) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, tx.driver+".latency",
		"db_name", tx.dbName,
		"method", method,
		"command", queryToCommand(query),
		"tx", "true",
		"stmt", "false",
	)
}

func (stmt Stmtx) reportLatency(ctx context.Context, method, query string) func(now time.Time) {
	return metrics.LatencyReporter(
		ctx, stmt.driver+".latency",
		"db_name", stmt.dbName,
		"method", method,
		"command", queryToCommand(query),
		"stmt", "true",
		"tx", strconv.FormatBool(stmt.fromTransaction),
	)
}
