# kit

Take care this library may move or be updated haphazardly, it's only purpose is for the tight5 cases in this org. It's based on various works, and may move to a better suited home subject to more stability. But for now it's here.

<p xmlns:dct="http://purl.org/dc/terms/">
  <a rel="license"
     href="http://creativecommons.org/publicdomain/zero/1.0/">
    <img src="https://licensebuttons.net/p/zero/1.0/88x31.png" style="border-style: none;" alt="CC0" />
  </a>
  <br />
  To the extent possible under law,
  <a rel="dct:publisher"
     href="https://gitlab.com/tight5">
    <span property="dct:title">Tight 5</span></a>
  has waived all copyright and related or neighboring rights to
  <span property="dct:title">Kit</span>.
  This work is published from:
<span property="vcard:Country" datatype="dct:ISO3166"
      content="US" about="https://github.com/zaquestion/lab">
  United States</span>.
</p>