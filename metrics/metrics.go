package metrics

import (
	"context"
	"fmt"
	"sync"
	"time"

	kitrics "github.com/go-kit/kit/metrics"
	"github.com/go-kit/kit/metrics/discard"
	"github.com/go-kit/kit/metrics/dogstatsd"
	"gitlab.com/tight5/kit/logger"
	"gitlab.com/tight5/kit/reqctx"
)

const (
	dockerDatadogAgentAddr = "172.17.0.1:8125"
	dogstatsdFlushTime     = time.Second * 5
)

var (
	dsd        *dogstatsd.Dogstatsd
	globalTags []string
	once       sync.Once
)

// StartDogstatsd returns a dogstatsd client that is flushing metrics to a
// docker datadog agent.
func StartDogstatsd(env, serviceName string) {
	switch env {
	case "stage", "prod", "production":
	default:
		logger.Warn().Log("msg", "Environment is not 'stage' or 'production', dogstatsd client disabled", "env", env)
		return
	}
	once.Do(func() { startDogstatsd(serviceName) })
}

func startDogstatsd(service string) {
	dsd = dogstatsd.New(fmt.Sprintf("service."), logger.Error())
	globalTags = []string{"service", service}

	ticker := time.NewTicker(dogstatsdFlushTime)
	ctx := context.Background()
	go dsd.SendLoop(ctx, ticker.C, "udp", dockerDatadogAgentAddr)
}

func Histogram(name string) kitrics.Histogram {
	if dsd == nil {
		return discard.NewHistogram()
	}
	return dsd.NewHistogram(name, 1).With(globalTags...)
}

func Gauge(name string) kitrics.Gauge {
	if dsd == nil {
		return discard.NewGauge()
	}
	return dsd.NewGauge(name).With(globalTags...)
}

func Counter(name string) kitrics.Counter {
	if dsd == nil {
		return discard.NewCounter()
	}
	return dsd.NewCounter(name, 1).With(globalTags...)
}

func CounterWithContext(ctx context.Context, name string) kitrics.Counter {
	return Counter(name).With(reqctx.MetricsTagsFromContext(ctx)...)
}

func LatencyReporter(ctx context.Context, name string, tags ...string) func(start time.Time) {
	return func(start time.Time) {
		h := Histogram(name).With(reqctx.MetricsTagsFromContext(ctx)...).With(tags...)
		h.Observe(time.Since(start).Seconds())
	}
}
