package middleware

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"runtime"
	"time"

	"gitlab.com/tight5/kit/logger"
	"gitlab.com/tight5/kit/metrics"
	"gitlab.com/tight5/kit/reqctx"

	"github.com/go-kit/kit/endpoint"
	"github.com/gogo/protobuf/jsonpb"
	"github.com/gogo/protobuf/proto"
)

type wrapper interface {
	WrapAllExcept(middleware endpoint.Middleware, excluded ...string)
	WrapAllLabeledExcept(middleware func(string, endpoint.Endpoint) endpoint.Endpoint, excluded ...string)
}

// Middlewares that are used by almost all truss services
// internals is for rpc names that do not need to pass through auth service (e.g. i-api)
func StandardMiddlewares(w wrapper) {
	w.WrapAllLabeledExcept(Latency(), "Status")
	w.WrapAllLabeledExcept(ErrorCounter(), "Status")
	w.WrapAllLabeledExcept(RequestLogger, "Status")
	w.WrapAllLabeledExcept(WrapContext)
	w.WrapAllExcept(Panic)
}

func Panic(in endpoint.Endpoint) endpoint.Endpoint {
	l := logger.With("middleware", "panic")
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		defer func() {
			if err := recover(); err != nil {
				buf := make([]byte, 4096)
				runtime.Stack(buf, false)
				l.Crit().Log("err", err, "stack", fmt.Sprintf("%s", buf[:bytes.IndexByte(buf, 0)]))
			}
		}()
		return in(ctx, req)
	}
}

// WrapContext puts request specific values into the context using keys that
// are specific to the service they were added from
func WrapContext(label string, in endpoint.Endpoint) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		ctx = context.WithValue(ctx, reqctx.NewContextKey(reqctx.ReqEndpoint), label)
		return in(ctx, req)
	}
}

func Latency(tags ...string) func(string, endpoint.Endpoint) endpoint.Endpoint {
	return func(label string, in endpoint.Endpoint) endpoint.Endpoint {
		h := metrics.Histogram("request.latency").With("endpoint", label).With(tags...)
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			defer func(begin time.Time) {
				h.With(reqctx.MetricsTagsFromContext(ctx)...).Observe(time.Since(begin).Seconds())
			}(time.Now())
			return in(ctx, req)
		}
	}
}

func ErrorCounter(tags ...string) func(string, endpoint.Endpoint) endpoint.Endpoint {
	return func(label string, in endpoint.Endpoint) endpoint.Endpoint {
		c := metrics.Counter("request.err").With("endpoint", label).With(tags...)
		return func(ctx context.Context, req interface{}) (interface{}, error) {
			resp, err := in(ctx, req)
			if err != nil {
				c.With(reqctx.MetricsTagsFromContext(ctx)...).Add(1)
			}
			return resp, err
		}
	}
}

func RequestLogger(label string, in endpoint.Endpoint) endpoint.Endpoint {
	return func(ctx context.Context, req interface{}) (interface{}, error) {
		resp, err := in(ctx, req)
		l := logger.Info()
		if err != nil {
			l = logger.Error()
		}

		pbM := jsonpb.Marshaler{
			OrigName:     true,
			EmitDefaults: false,
			EnumsAsInts:  false,
		}
		var (
			err2    error
			reqStr  string
			respStr string
		)
		if req != nil {
			reqStr, err2 = pbM.MarshalToString(req.(proto.Message))
			if err2 != nil {
				logger.Error().Log("msg", "failed marshaling request body", "err", err2)
			}
		}

		if resp != nil {
			respStr, err2 = pbM.MarshalToString(resp.(proto.Message))
			if err2 != nil {
				logger.Error().Log("msg", "failed marshaling response body", "err", err2)
			}
		}

		l.Log(
			"request.rpc", label,
			"request.url", ctx.Value("request-url"),
			"request.body", json.RawMessage([]byte(reqStr)),
			"response.body", json.RawMessage([]byte(respStr)),
			"response.error", err,
		)
		return resp, err
	}
}
