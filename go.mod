module gitlab.com/tight5/kit

go 1.12

require (
	github.com/DATA-DOG/go-sqlmock v1.3.3
	github.com/VividCortex/gohistogram v1.0.0 // indirect
	github.com/go-kit/kit v0.9.0
	github.com/go-logfmt/logfmt v0.4.0 // indirect
	github.com/go-stack/stack v1.8.0
	github.com/gogo/protobuf v1.3.1
	github.com/jmoiron/sqlx v1.2.0
	github.com/pkg/errors v0.8.1
	github.com/stretchr/testify v1.4.0
)

replace github.com/gogo/protobuf => github.com/metaverse/protobuf v1.3.1
